import requests
import json
from os import walk
import os
import re

# import pandas library as pd 
import pandas as pd

INPUT_FOLDER = "input"
OUTPUT_FOLDER = "output"

def load_filename():
    f = []
    for (dirpath, dirnames, filenames) in walk(INPUT_FOLDER):
    	print(filenames)
    	for filename in filenames:
    	  	f.append(os.path.join(INPUT_FOLDER,filename))
    return f


def generate_report():
    files = load_filename()
    print(files)

    total_reports = len(files)          
    print("Total reports : " + str(total_reports))

    count = 0
    for filename in files:
    	# Opening file 
    	file1 = open(filename, 'r')
    	output_file = os.path.basename(filename)
    	output_file = os.path.splitext(output_file)[0]
    	output_file = output_file + ".csv"
    	output_file = os.path.join(OUTPUT_FOLDER, output_file)
    	print(output_file)
    	# create an Empty DataFrame object
    	df = pd.DataFrame(columns = ["Filename", "Line Number", "Error Type", "Description", "Tools"])
    	print("\n\n*************** Generating Report *********************")
    	print(file1)
    	for line in file1:
    	    count += 1
    	    temp = {}
    	    #print("Processing Line {}: {}".format(count, line.strip()))
    	    line = line.strip()
    	    #print(line)
    	    split_line = line.split(":", 3)
    	    #print(split_line)
    	    if len(split_line) < 4:
    	    	continue
    	    temp["Filename"] = split_line[0]
    	    temp["Line Number"] = split_line[1]
    	    comments = split_line[-1]
    	    temp["Tools"] = comments.rsplit(" ", 1)[-1]
    	    comments = comments.rsplit(" ", 1)
    	    error_code = comments[0].strip().split(" ",1)
    	    #print(error_code)
    	    temp["Error Type"] = error_code[0]
    	    temp["Description"] = error_code[1]
    	    
    	    df = df.append(temp,ignore_index = True)
    	    #print(split_line)
    	    #print(len(split_line))
    	    
    	file1.close()
    	df.to_csv(output_file, index = False)

def generate_summary():
    print("*************** Generating Summary *********************")
    files = load_filename()
    print(files)

    total_reports = len(files)          
    print("Total Summary : " + str(total_reports))

    count = 0
    for filename in files:
    	# Opening file 
    	output_file = os.path.basename(filename)
    	output_file = os.path.splitext(output_file)[0]
    	output_file = output_file + ".csv"
    	output_file = os.path.join(OUTPUT_FOLDER, output_file)
    	print(output_file)
    	df = pd.read_csv(output_file)
    	# create an Empty DataFrame object
    	df_result = pd.DataFrame(columns = ["Error Type", "Description", "Count"])
    	print("\n\n*************** Generating Summary *********************")
    	error_details = df["Error Type"].value_counts()
    	error_list = list(error_details.index)
    	print(error_details)
    	#print(error_list)
    	completed_list = []
    	for items in error_details.iteritems():
            #print(items[0])
            if items[0] not in completed_list:
                for row in df.iterrows():
                    temp = {}
                    #print(row[1])
                    #print(row[1]["Filename"])
                    if row[1]["Error Type"] == items[0]:
                        temp["Error Type"] = row[1]["Error Type"]
                        temp["Description"] = row[1]["Description"]
                        temp["Count"] = items[1]
                        df_result = df_result.append(temp,ignore_index = True)
                        #print(temp)
                        completed_list.append(items[0])
                        break
    	output_file = os.path.basename(filename)
    	output_file = os.path.splitext(output_file)[0]
    	output_file = output_file + "_summary.csv"
    	output_file = os.path.join(OUTPUT_FOLDER, output_file)
    	df_result.to_csv(output_file, index = False)


def main():
    print("Inside main")
    generate_report()
    generate_summary()
    print("Exiting main")

if __name__ == '__main__':
    main()    
